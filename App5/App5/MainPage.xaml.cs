﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SQLite;

namespace App5
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        
        public List<Coach> CoachList;
        public int time = 1;
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            CoachList = await App.Database.GetCoachAsync();
            myListView.ItemsSource = await App.Database.GetCoachAsync();
        }
        
        async private void Loginbtn_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Email.Text) || string.IsNullOrEmpty(Password.Text))
                _ = DisplayAlert("Empty Values", "Please enter Email and Password", "OK");
            
            else
            {
                foreach(var c in CoachList)
                {
                    
                    time = 0;
                    if(Email.Text == c.Email && Password.Text == c.Password)
                    {
                        time ++;
                        _ = DisplayAlert("Login Success", "", "Ok");
                        await Navigation.PushModalAsync(new LoggedInPage());
                        break;
                    }                   
                }                
            }
            if (time == 0) { _ = DisplayAlert("Login Fail", "Please enter correct Email and Password", "OK"); }
        }

        async private void Registerbtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new RegisterPage());
        }         
    }
}
