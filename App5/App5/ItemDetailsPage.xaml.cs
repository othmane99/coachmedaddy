﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemDetailsPage : ContentPage
    {
        public ItemDetailsPage(string Name, string imgSrc, string Role, string Rank, int Id, string OpggUrl, string Price)
        {
            InitializeComponent();
            name.Text = Name;
            img.Source = imgSrc;
            role.Text = Role;
            rank.Text = Rank;
            opggUrl.Text = OpggUrl;
            price.Text = Price + " per hour";
        }
    }
}