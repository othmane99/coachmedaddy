﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App5
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            collectionView.ItemsSource = await App.Database.GetCoachAsync();
        }

        async void OnButtonClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(NewEmail.Text) || string.IsNullOrEmpty(NewPassword.Text) )
            {
                _=DisplayAlert("You can't register", "Pease fill all the informations !", "OK");
    
            }
            if ((NewPassword.Text != ConfirmPassword.Text))
            {
                _ = DisplayAlert("You can't register", "Pease check your password !", "OK");
            }
            else
            {
                
                if (!string.IsNullOrWhiteSpace(NewEmail.Text))
                {
                    await App.Database.SaveCoachAsync(new Coach
                    {
                        Email = NewEmail.Text,
                        Password = NewPassword.Text,
                        Name = NewName.Text,
                        Rank = NewRank.Text,
                        Role = NewRole.Text
                    });

                    NewEmail.Text = string.Empty;
                    NewPassword.Text = string.Empty;
                    NewName.Text = string.Empty;
                    NewRank.Text = string.Empty;
                    NewRole.Text = string.Empty;

                    collectionView.ItemsSource = await App.Database.GetCoachAsync();
                    _=DisplayAlert("You are now registred", "Enter your Email and Password to log In", "OK");
                    await Navigation.PushModalAsync(new MainPage());
                }
                
            }
            
        }
    }
    
}