﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace App5
{
    public class Database
    {
        private readonly  SQLiteAsyncConnection _connection;
        public Database(string dbPath)
        {
            _connection = new SQLiteAsyncConnection(dbPath);
            _connection.CreateTableAsync<Coach>();

        }

        public Task<List<Coach>> GetCoachAsync()
        {
            return _connection.Table<Coach>().ToListAsync();
        }
        public Task<int> SaveCoachAsync(Coach coach)
        {
            return _connection.InsertAsync(coach);
        }
    }
}
