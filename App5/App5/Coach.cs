﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace App5
{
    public class Coach
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public string Role { get; set; }
        
        //public DateTime Created { get; set; }
        
       
    }
}
