﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App5
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Created { get; set; }
        public string Rank { get; set; }
        public string Role { get; set; }
        public string OpggUrl { get; set; }
        public string Img { get; set; }
        public string RankObj { get; set; }
        public string Price { get; set; }
        public bool IsPassword { get; set; }
        public User(int id, string name, string role, string rank, string img, string price, string opggUrl)
        {
            Id = id;
            Name = name;
            Role = role;
            Rank = rank;
            Img = img;
            Price = price;
            OpggUrl = opggUrl;
        }
    }
}
