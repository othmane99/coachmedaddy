﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoggedInPage : ContentPage
    {
        public List<User> Users { get; set; } = new List<User>();


        public LoggedInPage()
        {
            InitializeComponent();
            //string a = "https://th.bing.com/th/id/R.c5c74a54e538eac61f31bc5936e7aab0?rik=frs3C8E3IflnFA&pid=ImgRaw&r=0";
            Users.Add(new User(1,"JhinIc0l0gue","ADC","Plat", "Assets/jhinIc0l0gue.png", "10 $","https://op.gg"));
            Users.Add(new User(2,"SpouXX","MID", "Diam", "SpouXX.png", "10 $", "https://www.op.gg"));
            Users.Add(new User(3,"Rekkless","ADC/SUPP", "Gold", "rekkles.png", "20 $", "https://www.op.gg"));
            Users.Add(new User(4,"Dopa","ALL", "Silver", "dopa.png", "25 $", "https://www.op.gg"));
            Users.Add(new User(5, "Hide on Bush", "ALL", "Challenger", "Hideonbush.png", "10 $", "https://www.op.gg/summoners/kr/Hide%20on%20bush"));
            Users.Add(new User(6, "Doublelift", "ADC", "Grand MAster", "doublelift.png", "20 $", "https://www.op.gg"));
            Users.Add(new User(7, "Shake", "JGL", "Plat", "shake.png", "5 $", "https://www.op.gg"));
            Users.Add(new User(8, "MikyX", "SUPP", "Silver", "mikyx.png", "20 $", "https://www.op.gg"));
            Users.Add(new User(9, "ynss", "TOP", "Bronze", "ynss.png", "1 $", "https://www.op.gg"));
            Users.Add(new User(10, "LonzoBall", "SUPP", "Challenger", "LonzoBall.png", "100 $", "https://www.op.gg"));
            Users.Add(new User(11, "Caps", "MID", "Master", "caps.png", "20 $", "https://www.op.gg"));
            Users.Add(new User(12, "Skullmars", "ALL", "Silver", "Skullmars.png", "25 $", "https://www.op.gg"));
            Users.Add(new User(13, "Kamalovic", "ALL", "Challenger", "kamalovic.png", "25 $", "https://www.op.gg"));

            BindingContext = this;
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            myListView.ItemsSource = Users.Where(s => s.Name.ToLower().StartsWith(e.NewTextValue.ToLower()));
        }
        

        private void MySelectedListView_ItemSelected(object sender, SelectedPositionChangedEventArgs e)
        {
            
        }

        async private void MySelectedListView(object sender, SelectedItemChangedEventArgs e)
        {
            var item = ((ListView)sender).SelectedItem;
            var index = Users.IndexOf((User)item);
            var imgSrc = Users[index].Img;
            var Name = Users[index].Name;
            var Role = Users[index].Role;
            var Rank = Users[index].Rank;
            var Id = Users[index].Id;
            var OpggUrl = Users[index].OpggUrl;
            var Price = Users[index].Price;
            //await Navigation.PushAsync(new ItemDetailsPage(Name, imgSrc, Role, Rank, Id, OpggUrl,Price), true);
            await Navigation.PushModalAsync(new ItemDetailsPage(Name, imgSrc, Role, Rank, Id, OpggUrl, Price), true);

        }

    }
}